export class Grade {
    constructor(courseName, gradeValue, examValue) {
        this.courseName = courseName;
        this.gradeValue = gradeValue;
        this.examValue = examValue;
    }

    setGradeValue(newGradeValue) {
        this.gradeValue = newGradeValue;
    }

}