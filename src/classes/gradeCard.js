import { Grade } from "./grade";

export class GradeCard {

    static currentGradeCard = [];
    static currentYear = this.getCalcYear() + 1;

    static getGrades() {
        let arr = JSON.parse(localStorage.getItem("grades"));
        if (arr === null) return false;
        this.currentGradeCard = arr;
        this.currentYear = JSON.parse(localStorage.getItem("currentYear"));

    }

    static setGrades() {
        localStorage.setItem("grades", JSON.stringify(this.currentGradeCard))
        localStorage.setItem("currentYear", JSON.stringify(this.currentYear))
    }

    static addGrade(gradeName, newValue) {
        let registered = false;

        for (let i = 0; i < this.currentGradeCard.length; i++) {
            if (gradeName == this.currentGradeCard[i].courseName) {
                this.currentGradeCard[i].gradeValue = newValue;
                registered = true
            }
        }

        if (!registered) {
            let newGrade = new Grade(gradeName, newValue, -1);
            this.currentGradeCard.push(newGrade);
            registered = true;
        }
    }

    static addExam(gradeName, newValue) {
        let registered = false;

        for (let i = 0; i < this.currentGradeCard.length; i++) {
            if (gradeName == this.currentGradeCard[i].courseName) {
                this.currentGradeCard[i].examValue = newValue;
                registered = true
            }
        }

        if (!registered) {
            let newGrade = new Grade(gradeName, -1, newValue);
            this.currentGradeCard.push(newGrade);
            registered = true;
        }
    }

    static addYearBorn(year) {
        this.currentYear = year;
        this.setGrades();
        this.getGrades();
    }

    static getSingleGrade(gradename) {
        for (let i = 0; i < this.currentGradeCard.length; i++) {
            if (gradename == this.currentGradeCard[i].courseName) {
                return this.currentGradeCard[i].gradeValue;

            }
        }
    }

    static getSingleExam(gradename) {
        for (let i = 0; i < this.currentGradeCard.length; i++) {
            if (gradename == this.currentGradeCard[i].courseName) {
                return this.currentGradeCard[i].examValue;
            }
        }
    }

    static getCurrentYear() {
        return this.currentYear;
    }

    static resetGrades() {
        alert("Er du sikker på at du ønsker å fjerne alle karakterene?")
        localStorage.removeItem("grades");
        localStorage.removeItem("currentYear");
        this.getGrades();
        window.location.reload();
    }

    static getAverage() {
        if (this.currentGradeCard.length < 1) return 0;

        let total = 0;
        let totalAmount = 0


        for (let i = 0; i < this.currentGradeCard.length; i++) {
            let grade = this.currentGradeCard[i];

            if (grade.gradeValue !== -1) {
                total += grade.gradeValue;
                totalAmount++;
            }

            if (grade.examValue !== -1) {
                total += grade.examValue;
                totalAmount++;
            }

        }
        let number = (total / totalAmount) * 10;
        const str_a = number.toString();
        return Number(str_a.slice(0, 4));
    }

    static getAverageCompetitionPoints() {
        let points = this.getAverage() + this.calculateAgePoints();

        return points;
    }

    static calculateAgePoints() {
        if (this.currentYear === (this.getCalcYear() + 1)) {
            return 0;
        }
        else if (this.currentYear === (this.getCalcYear())) {
            return 2;
        }
        else if (this.currentYear === (this.getCalcYear() - 1)) {
            return 4;
        }
        else if (this.currentYear === (this.getCalcYear() - 2)) {
            return 6;
        }
        else if (this.currentYear === (this.getCalcYear() - 3)) {
            return 8;
        }
    }

    static getCalcYear() {
        let year = new Date().getFullYear();
        return year - 20;
    }
}

export default GradeCard;