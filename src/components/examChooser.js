
import React from 'react';
import { Form, ButtonToolbar, Container, Card, Button, ButtonGroup, Col, Row } from "react-bootstrap";
import { GradeCard } from '../classes/gradeCard';

export class ExamChooser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            course: this.props.course,

        }

    }

    componentDidMount() {
        GradeCard.getGrades();
    }

    gradeDisplay() {
        if (GradeCard.getSingleExam(this.props.course) === undefined) return "-";
        if (GradeCard.getSingleExam(this.props.course) < 0) return "-";
        return GradeCard.getSingleExam(this.props.course);
    }


    render() {
        return (

            <Row>

                <Col>
                    <Card className="border-0" bg="secondary">
                        <h3>{this.gradeDisplay()}</h3>
                    </Card>
                </Col>

                <Col>

                    <ButtonGroup>
                        <Button onClick={(e) => this.selectItem(1)} variant={this.chooseColor(1)}>1</Button>
                        <Button onClick={(e) => this.selectItem(2)} variant={this.chooseColor(2)}>2</Button>
                        <Button onClick={(e) => this.selectItem(3)} variant={this.chooseColor(3)}>3</Button>
                        <Button onClick={(e) => this.selectItem(4)} variant={this.chooseColor(4)}>4</Button>
                        <Button onClick={(e) => this.selectItem(5)} variant={this.chooseColor(5)}>5</Button>
                        <Button onClick={(e) => this.selectItem(6)} variant={this.chooseColor(6)}>6</Button>
                    </ButtonGroup>
                </Col>

            </Row>
        )
    }

    chooseColor(number) {
        if (number === GradeCard.getSingleExam(this.props.course)) {
            return "primary";
        }
        else {
            return "dark";
        }
    }

    selectItem(inputValue) {
        this.setState({ examValue: inputValue });
        GradeCard.addExam(this.state.course, inputValue);
        this.props.triggerParentUpdate();
    }


}



export default ExamChooser;