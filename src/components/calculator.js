
import React from 'react';
import { Form, Col, Row, Card, Button, Spinner, Accordion } from "react-bootstrap";
import GradeChooser from "./gradeChooser";
import GradeCard from '../classes/gradeCard';
import BoolChooser from './boolChooser';
import AgeChooser from './ageChooser';
import ExamChooser from './examChooser';
import { SearchList } from './Search';
let mainCourses = require('../resources/mainCourses.json');

export class Calculator extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            addCourseArray: [],
            addCourseName: '',
            averageScore: GradeCard.getAverage(),
            showExtraPoints: false,
            averageCompetitionPoints: GradeCard.getAverageCompetitionPoints()
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    render() {
        return (
            <Card bg="dark" className="p-3 mt-5 text-center mx-auto shadow-lg">
                <Row>
                    <Col>
                        <Card className="p-2 border-0" bg="dark">
                            <Card.Body>
                                <Card.Title className="font-italic text-white mx-auto">"ANNESJ"</Card.Title>
                                <Card.Text className="font-italic text-white mx-auto">PROFESJONELL KARAKTERKALKULATOR</Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>

                    <Col>
                        <Card className="border-0 p-2 shadow mt-4 mx-auto text-white" bg={this.chooseColor()}>
                            <Card.Title>SKOLEPOENG</Card.Title>
                            <h2 >{this.state.averageScore}</h2>
                            <Form.Text>FØRSTEGANGSVITNEMÅL</Form.Text>
                        </Card>

                    </Col>
                    <Col>

                        <Card className="border-0 p-2 shadow mt-4 mx-auto text-white" bg={this.chooseColor()}>
                            <Card.Title>KONKURRANSEPOENG</Card.Title>
                            <h2 >{this.state.averageCompetitionPoints}</h2>
                            <Form.Text>ORDINÆR KVOTE</Form.Text>
                        </Card>
                    </Col>
                </Row>

                <br />
                <Form onSubmit={this.handleSubmit}>

                    <Card bg="secondary" className="p-4 m-1 shadow text-white">
                        <Row hidden={this.isMobile()}>
                            <Col><Card.Title className="text-left">Fag</Card.Title></Col>
                            <Col><Card.Title>Standpunkt</Card.Title></Col>
                            <Col><Card.Title>Eksamen</Card.Title></Col>
                            <hr />
                        </Row>


                        
                        {mainCourses.map(course =>
                            <Row className="mt-2">
                                <Col hidden={this.isMobile()}>
                                    <p class="text-justify">{course.name}</p>
                                </Col>
                                <Col>
                                    <p hidden={!this.isMobile()} class="">{course.name}</p>
                                    <GradeChooser key={course.name} showLabel={course.showLabel} triggerParentUpdate={this.updateInfo} course={course.name} extraPoints={course.extraPoints} />
                                    <hr hidden={!this.isMobile()}/>
                                </Col>


                                <Col>
                                    <p hidden={!this.isMobile()} class="">{course.name} eksamen</p>
                                    <ExamChooser key={course.name} showLabel={course.showLabel} triggerParentUpdate={this.updateInfo} course={course.name} extraPoints={course.extraPoints} />
                                    <hr hidden={!this.isMobile()}/>
                                </Col>
                                
                            </Row>
                            

                        )}


                    </Card>

                    
                    <Row hidden={!this.state.showExtraPoints}>

                        <Col>
                            <Card bg="secondary" className="p-4 m-1 mt-2 shadow text-white text-left">
                                <Card.Title>Realfag</Card.Title>
                                <hr />
                                <SearchList list={mainCourses}/>
                            </Card></Col>

                        <Col>
                            <Card bg="secondary" className="p-4 m-1 mt-2 shadow text-white text-left">
                                <Card.Title>Fagbrev og andre poeng</Card.Title>
                                <hr />
                                <AgeChooser showLabel={true} triggerParentUpdate={this.updateInfo} course="Alderspoeng" />

                            </Card></Col>
                    </Row>
                </Form>
        
                <Button variant="success" className="shadow m-1 float-left" hidden={this.state.showExtraPoints} onClick={() => this.exeExtraPoints()}>Legg til realfagspoeng og ekstrapoeng</Button>
                <Button variant="danger" className="shadow float-right mt-4  m-1" onClick={() => GradeCard.resetGrades()}>Slett alle karakterer</Button>
                <Button hidden={!this.isMobile()} variant="primary" className="shadow float-right mt-4" onClick={() => window.scrollTo(0,0)}>Scroll til toppen</Button>
            </Card>

        )
    }



    isMobile(){
       return window.innerWidth <= 500;
    }
    exeExtraPoints() {
        this.setState({ showExtraPoints: true });
    }

    handleInputChange(event) {

        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({ [name]: value, });
    }

    addCourseEvent() {
        alert(this.state.addCourseName)

    }


    componentDidMount() {
        this.updateInfo();
    }

    updateInfo = () => {
        GradeCard.setGrades();
        GradeCard.getGrades();
        this.setState({ averageScore: GradeCard.getAverage() });
        this.setState({ averageCompetitionPoints: GradeCard.getAverageCompetitionPoints() });
    }

    chooseColor() {
        if (this.state.averageScore === 0) return "secondary";
        else { return "info" };

    }

}






export default Calculator;