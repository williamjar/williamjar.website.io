import React, {useState} from 'react';
import { Dropdown, Col, Row, Button, Card, FormControl } from "react-bootstrap";
let mainCourses = require('../resources/mainCourses.json');

export const SearchBar = ({ childSearchTerm, handleSearch }) => {
    
    return(
        <div>
            <FormControl 
                placeholder={"Søk her"}
                value={childSearchTerm} 
                onChange={handleSearch}>
            </FormControl>
     </div>

    )
}

export const List = ({ sortedItems }) => {
    
    return(
        <div>
            {sortedItems.map((item) => 
            <Card bg="dark" className="mt-1 p-2">
                
            <Row>
                <Col>
                <p className="float-left text-white">{item.name}</p>
                
                </Col>
                <Col >
                    <Button className="float-right" variant="success">Legg til</Button>
                </Col>
                </Row>
                </Card>
                )}   
     </div>

    )
}

function sortTableBySearchTerm (list, searchTerm) {
    let sorted = [];

    if(searchTerm.length===0) return sorted;

    for (let i = 0; i < list.length; i++) {

        let name = list[i].name;

        if (name.toLowerCase().startsWith(searchTerm.toLowerCase())){
            sorted.push(list[i]);
        }
    }
    return sorted.slice(0,4);
}


export const SearchList = ({ list }) => {
    
    const [childSearchTerm, setChildSearchTerm] = useState('');
    
    const handleSearch = event => {
        setChildSearchTerm(event.target.value)
    }

    return(
        <div>
            <SearchBar handleSearch={handleSearch} childSearchTerm={childSearchTerm}/>
            <List sortedItems={sortTableBySearchTerm(list,childSearchTerm)}/>
        </div>

    )
}



