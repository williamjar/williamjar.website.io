
import React from 'react';
import { Form, ButtonToolbar, Container, Button, ButtonGroup, Col, Row, Card } from "react-bootstrap";
import { GradeCard } from '../classes/gradeCard';

export class BoolChooser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            course: this.props.course,
            gradeValue: GradeCard.getSingleExam(this.props.course),
            switch: 2
        }
    }

    componentDidMount() {
        GradeCard.getGrades();
    }

    gradeDisplay() {
        if (GradeCard.getSingleGrade(this.props.course) === undefined) return "-";
        if (GradeCard.getSingleGrade(this.props.course) < 0) return "-";
        return GradeCard.getSingleGrade(this.props.course);
    }

    render() {
        return (

            <Row className="mt-4">
                <Col>

                    {this.props.showLabel ? (<p>{this.state.course}</p>) : null}

                </Col>


                <Col>
                    <h3 hidden={this.state.switch === 2}>+{this.gradeDisplay()}</h3>
                    <h3 hidden={this.state.switch === 1}>-</h3>
                </Col>

                <Col>
                    <ButtonGroup>
                        <Button onClick={(e) => this.selectItem(1)} variant={this.chooseColor(1)}>Ja</Button>
                        <Button onClick={(e) => this.selectItem(2)} variant={this.chooseColor(2)}>Nei</Button>
                    </ButtonGroup>
                </Col>
            </Row>
        )
    }

    chooseColor(number) {
        if (number === this.state.switch) {
            if (number == 2) {
                return "danger";
            }
            return "primary";
        }
        else {
            return "dark";
        }
    }


    selectItem() {

        if (this.state.switch === 2) {
            GradeCard.addExtraPoints(this.state.gradeValue);
            this.props.triggerParentUpdate();
            this.setState({ switch: 1 });
        }

        if (this.state.switch === 1) {
            GradeCard.removeExtraPoints(this.state.gradeValue);
            this.props.triggerParentUpdate();
            this.setState({ switch: 2 });
        }


    }



}



export default BoolChooser;