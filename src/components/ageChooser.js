
import React from 'react';
import { Dropdown, Col, Row, Card } from "react-bootstrap";
import { GradeCard } from '../classes/gradeCard';

export class AgeChooser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            course: this.props.course,
        }
    }

    componentDidMount() {
        GradeCard.getGrades();
    }

    render() {
        return (
            <Row className="mt-4">
                <Col>
                    <p className="text-justify">{this.props.course}</p>
                </Col>

                <Col>
                    <h3>{GradeCard.calculateAgePoints()}</h3>
                </Col>

                <Col>
                    <div style={{ width: '10rem' }}>
                    <Dropdown>
                        <Dropdown.Toggle variant={this.chooseColor()} id="dropdown-basic">
                            {this.display(GradeCard.getCurrentYear())}
                        </Dropdown.Toggle>

                        <Dropdown.Menu >
                            <Dropdown.Item onClick={() => this.chooseYear(this.currentYear() + 1)} >{this.display(this.currentYear() + 1)}</Dropdown.Item>
                            <Dropdown.Item onClick={() => this.chooseYear(this.currentYear())} >{this.display(this.currentYear())}</Dropdown.Item>
                            <Dropdown.Item onClick={() => this.chooseYear(this.currentYear() - 1)}>{this.display(this.currentYear() - 1)}</Dropdown.Item>
                            <Dropdown.Item onClick={() => this.chooseYear(this.currentYear() - 2)} >{this.display(this.currentYear() - 2)}</Dropdown.Item>
                            <Dropdown.Item onClick={() => this.chooseYear(this.currentYear() - 3)} >{this.display(this.currentYear() - 3)}</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    </div>
                </Col>
                
            </Row>
        )
    }

    chooseYear(year) {
        GradeCard.addYearBorn(year);
        this.props.triggerParentUpdate();
    }

    chooseColor() {
        return "primary";
    }

    currentYear() {
        let year = new Date().getFullYear();
        return year - 20;
    }

    display(year) {

        if (this.currentYear() + 1 === year) {
            return year + " og senere";
        }

        if (this.currentYear() - 3 === year) {
            return year + " og tidligere";
        }

        return year;

    }
}



export default AgeChooser;